Download
========

You can obtain a copy of LIBPFASST through the

`PFASST@lbl.gov project page`_

or directly from `bitbucket.org`_.

In either case, you will get the source code for the library, Makefiles, tutorials, and the files from which the documentation is made.







.. _`PFASST@lbl.gov project page`: https://pfasst.lbl.gov
.. _`bitbucket.org`: https://bitbucket.org/berkeleylab/libpfasst/src/master/



